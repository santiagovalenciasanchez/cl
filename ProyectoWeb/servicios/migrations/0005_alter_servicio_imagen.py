# Generated by Django 3.2.6 on 2022-02-10 01:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0004_auto_20211017_1041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicio',
            name='imagen',
            field=models.FileField(upload_to='servicios'),
        ),
    ]
