# Generated by Django 3.2.6 on 2021-10-12 21:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicio',
            name='contenido',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='servicio',
            name='titulo',
            field=models.CharField(max_length=45),
        ),
    ]
