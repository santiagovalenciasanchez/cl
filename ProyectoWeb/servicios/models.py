from django.db import models

# Create your models here.

class Servicio(models.Model):
    titulo=models.CharField(max_length=45)
    contenido=models.CharField(max_length=200)
    imagen=models.CharField(verbose_name='servicios',max_length=255)
    created=models.DateTimeField(auto_now_add=True)
    updated=models.DateTimeField(auto_now_add=True)

    class meta:
        verbose_name='servicio'
        verbose_name_plural='servicios'
    def __str__(self):
        return self.titulo
     

        
