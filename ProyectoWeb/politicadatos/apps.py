from django.apps import AppConfig


class PoliticaDatosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'politicadatos'
