# Generated by Django 3.2.6 on 2022-03-08 17:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0036_auto_20220307_1423'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='categorias',
            field=models.ManyToManyField(to='blog.Categoria'),
        ),
        migrations.AlterField(
            model_name='post',
            name='contenido',
            field=models.CharField(max_length=250),
        ),
    ]
