# Generated by Django 3.2.6 on 2021-10-18 13:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0026_alter_post_categorias'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='categorias',
            field=models.ManyToManyField(to='blog.Categoria'),
        ),
    ]
