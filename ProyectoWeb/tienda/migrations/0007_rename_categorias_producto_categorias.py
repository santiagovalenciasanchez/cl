# Generated by Django 3.2.6 on 2021-10-17 21:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tienda', '0006_alter_producto_options'),
    ]

    operations = [
        migrations.RenameField(
            model_name='producto',
            old_name='categorias',
            new_name='Categorias',
        ),
    ]
