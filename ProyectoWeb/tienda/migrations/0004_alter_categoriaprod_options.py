# Generated by Django 3.2.6 on 2021-10-17 16:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tienda', '0003_auto_20211017_1041'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='categoriaprod',
            options={'verbose_name': 'CategoriaProd', 'verbose_name_plural': 'categoriasProd'},
        ),
    ]
