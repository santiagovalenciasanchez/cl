import django
from django.shortcuts import render, redirect
from .forms import formulariocontacto
from django.core.mail import EmailMessage

# Create your views here.
def contacto(request):
    fromulario_contacto=formulariocontacto()

    if request.method=="POST":
        formulario_contacto=formulariocontacto(data=request.POST)
        if formulario_contacto.is_valid():
            nombre=request.POST.get("nombre")
            email=request.POST.get("email")
            telefono=request.POST.get("telefono") 
            contenido=request.POST.get("contenido")
            email=EmailMessage("mensaje enviado desde pagina web CL LA CASA DEL MARMOL Y EL GRANITO",
            "El USUARIO CON EL NOMBRE : {} \n\nCON LA DIRECCION DE CORREO : {}\nCON EL NUMERO DE TELEFONO : {}\nESCRIBE LO SIGUIENTE:\n\n {}".format(nombre,email,telefono,contenido),
            "",["cl.lacasadelmarmol@gmail.com"],reply_to=[email])

            try:
                email.send()
                return redirect("/contacto/?envio_exictoso")
            except:
                return redirect("/contacto/?envio_no_exictoso")    


    return render(request,'contacto/contacto.html',{'miformulario':fromulario_contacto})
