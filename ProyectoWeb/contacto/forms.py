from django import forms

class formulariocontacto(forms.Form):

    nombre=forms.CharField(label="nombre", required=True)
    email=forms.CharField(label="email", required=True)
    telefono=forms.IntegerField(label="telefono", required=True)
    contenido=forms.CharField(label="contenido",widget=forms.Textarea)


    